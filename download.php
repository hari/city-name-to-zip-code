
application/x-httpd-php download.php ( PHP script text )

<?php
require_once __DIR__ .'/autoload.php';
require_once __DIR__ .'/vendor/autoload.php';
require_once __DIR__ .'/src/db_config.php';

$rows = DataTable::getFilled();

$file = fopen('data.csv', 'w+');
fputcsv($file, ['name', 'ortname', 'country' , 'plz']);

foreach($rows as $row) {
  $row = fix($row);
  $dups = explode(' ', $row[3]);
  foreach($dups as $dup) {
    fputcsv($file, [$row[0], $row[1], $row[2], $dup]);
  }
}
fclose($file);

function fix($row) {
  $row['name'] = html_entity_decode($row['name']);
  $row['ortname'] = html_entity_decode($row['ortname']);
  return [$row['name'], $row['ortname'], $row['country'], trim(str_replace(',', ' ', $row['plz']))];
}

header('Location: data.csv');

