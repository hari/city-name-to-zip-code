<?php

class Data
{
  private $data = [];

  public function __construct()
  {

  }

  public function set($name, $value)
  {
    $map = [
      'A' => 'name',
      'B' => 'ortname',
      'country' => 'country'
    ];
    if (array_key_exists($name, $map)) {
      $this->data[$map[$name]] = $value;
    }
  }

  public function __get($name) {
    if (array_key_exists($name, $this->data)) {
      return htmlentities($this->data[$name], ENT_QUOTES);
    }
    return -1;
  }

}