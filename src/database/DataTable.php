<?php

class DataTable
{

    public static function create()
    {
        $sql = "CREATE TABLE `".DATA_TABLE_NAME."` ( 
                    `row_id` INT NOT NULL AUTO_INCREMENT , 
                    `name` VARCHAR(255) NOT NULL ,
                    `plz` VARCHAR(255) NULL DEFAULT NULL, 
                    `ortname` VARCHAR(255) NOT NULL ,
                    `country` VARCHAR(255) NOT NULL ,
                    `executed_on` DATETIME NOT NULL ,
                    PRIMARY KEY (`row_id`));";
        return Connection::execute($sql);
    }

    public static function insert($data)
    {
        if (!is_object($data)) {
            throw new Exception('data must be instance if models\Data');
        }
        $sql = sprintf('INSERT INTO `%s` (`name`, `plz`, `ortname`, `country`) 
               VALUES (:vor, :plz, :ort, :country, )', DATA_TABLE_NAME);
        $stm = Connection::get()->prepare($sql);
        $stm->bindParam(':name', $data->name, PDO::PARAM_STR);
        $stm->bindParam(':plz', $data->plz, PDO::PARAM_STR);
        $stm->bindParam(':ort', $data->ortname, PDO::PARAM_STR);
        $stm->bindParam(':country', $data->country, PDO::PARAM_INT);
        return $stm->execute();
    }

    public static function insertBulk($raw_sql)
    {
        $sql = sprintf('INSERT INTO `%s` (`name`, `plz`, `ortname`, `country`) VALUES %s', DATA_TABLE_NAME, $raw_sql);
        return Connection::get()->prepare($sql)->execute();
    }

    public static function getById($id)
    {
        $sql = sprintf('SELECT * FROM `%s` WHERE `row_id` = :id', DATA_TABLE_NAME);
        $stm = Connection::get()->prepare($sql);
        $stm->bindParam(':id', $name, PDO::PARAM_STR);
        if ($stm->execute()) {
            return $stm->fetch();
        }
        return null;
    }

    public static function getCount($column) {
        $sql = sprintf('SELECT COUNT(DISTINCT %s) FROM %s', $column, DATA_TABLE_NAME);
        $stm = Connection::get()->prepare($sql);
        if ($stm->execute()) {
            $result = $stm->fetch();
            if (count($result) > 0) {
                return $result[array_keys($result)[0]];
            }
        }
        return 0;
    }

    public static function delete() {
        $sql = 'DELETE FROM '. DATA_TABLE_NAME;
        return Connection::get()->prepare($sql)->execute();
    }

    public static function getNextCities()
    {
        $city_sql = sprintf('SELECT DISTINCT(ortname), plz, country FROM %s WHERE executed_on is NULL LIMIT 5', DATA_TABLE_NAME);
        $stmt = Connection::get()->prepare($city_sql);
        if ($stmt->execute()) {
            $rows = array_map(function ($item) {
                return ['plz' => $item['plz'], 'name' => html_entity_decode($item[array_keys($item)[0]]), 'country' => $item['country']];
            }, $stmt->fetchAll());
            return $rows;
        }
        return [];
    }

    public static function updateCode($city_name, $postal_codes)
    {
        if (strpos($city_name, '&#039;') > 0 || strpos(htmlentities($city_name, ENT_QUOTES), '&#039;') > 0) {
            $city_name = strpos($city_name, '&#039;') > 0 ? $city_name : htmlentities($city_name, ENT_QUOTES);
            $sql = sprintf("UPDATE %s SET plz = '%s', executed_on = '%s' WHERE ortname = '%s'", DATA_TABLE_NAME, $postal_codes, date('Y-m-d H:i:s'), $city_name);
            return Connection::get()->prepare($sql)->execute();
        }

        $sql = sprintf('UPDATE %s SET plz = :codes, executed_on = :ex WHERE ortname = :city', DATA_TABLE_NAME);
        $city_name = htmlentities($city_name, ENT_QUOTES);
        $stmt = Connection::get()->prepare($sql);
        $stmt->bindParam(':codes', $postal_codes, PDO::PARAM_STR);
        $now = date('Y-m-d H:i:s');
        $stmt->bindParam(':ex', $now, PDO::PARAM_STR);
        $stmt->bindParam(':city', $city_name, PDO::PARAM_STR);
        return $stmt->execute();
    }

    public static function all()
    {
        $sql = sprintf('SELECT * FROM %s GROUP BY ortname', DATA_TABLE_NAME);
        $stmt = Connection::get()->prepare($sql);
        if ($stmt->execute()) {
            return $stmt->fetchAll();
        }
        return [];
    }

    public static function getFilled()
    {
        $sql = sprintf("SELECT name, ortname, plz, country FROM %s WHERE plz NOT IN ('0','-1','1')", DATA_TABLE_NAME);
        $stmt = Connection::get()->prepare($sql);
        if ($stmt->execute()) {
            return $stmt->fetchAll();
        }
        return []; 
    }
}
