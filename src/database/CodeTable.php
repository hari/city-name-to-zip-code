<?php
/**
 * Class representing the table which stores the offline postal data
 * these data will be stored in the city later.
 */
class CodeTable
{
  public static function create()
  {
    $sql = 'CREATE TABLE `holder` ( `id` INT NOT NULL AUTO_INCREMENT , `country` VARCHAR(255) NOT NULL , `code` VARCHAR(255) NOT NULL , `place` VARCHAR(255) NOT NULL , `a` VARCHAR(255) NOT NULL , `b` VARCHAR(255) NOT NULL , `c` VARCHAR(255) NOT NULL , `d` VARCHAR(255) NOT NULL , `e` VARCHAR(255) NOT NULL , `f` VARCHAR(255) NOT NULL , `g` VARCHAR(255) NOT NULL , `h` VARCHAR(255) NOT NULL , `i` VARCHAR(255) NOT NULL , PRIMARY KEY (`id`))';
  }

  public static function getCode($country, $city)
  {
    $city = htmlentities($city);
    $sql = sprintf("SELECT code FROM %s WHERE country = :country AND place = :city", CODE_TABLE_NAME);
    $stmt = Connection::get()->prepare($sql);
    $stmt->bindParam(':country', $country, PDO::PARAM_STR);
    $stmt->bindParam(':city', $city, PDO::PARAM_STR);
    if ($stmt->execute()) {
      return array_map(function ($data) {
        return $data['code'];
      }, $stmt->fetchAll());
    }
    return array();
  }
  
  public static function fix($id)
  {
    $sql = "select distinct(place), id from holder where i = '-1' AND country = 'DE' AND id > $id limit 500";
    $stmt = Connection::get()->prepare($sql);
    $count = 0;
    if ($stmt->execute()) {
      $cities = $stmt->fetchAll();
      foreach($cities as $city) {
        $tcity = htmlentities($city['place'], ENT_QUOTES);
        if ($tcity != $city['place']) {
          if (self::update($city['id'], $tcity)) {
            $count++;
          }
        }
      }
      $id = end($cities)['id'];
    } else {
      var_dump($stmt->errorInfo());
    }
    echo 'affected: '.$count;
    return $id;
  }
  
  private static function update($id, $html_city)
  {
    $sql = "update holder set place = '$html_city', `i` = '1' where id = $id";
    $stmt = Connection::get()->prepare($sql);
    if (!$stmt->execute()) {
      echo $sql;
      return false;
    }
    return true;
  }
}