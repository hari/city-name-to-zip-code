<?php

use PhpOffice\PhpSpreadsheet\IOFactory;

class Utils
{
    static $allowed_extensions = ['xls', 'xlsx', 'csv'];
    static $dir = 'public/uploads';
    
  public static function next()
  {
    $cities = DataTable::getNextCities();
    $request = new ApiRequest();
    foreach ($cities as $city) {
      if ($city['plz'] != 0 && $city['plz'] != -1) {
        echo 'Used both of the source already.';
      } else {
        $codes = CodeTable::getCode($city['country'], $city['name']);
        if (empty($codes)) {
          echo '<p>'.self::filterAndUpdate($city['name'], 
                      $request->pullData($city['name'], $city['country'], $city['plz'] == -1),
                      $city['plz']).'</p>';
        } else {
          echo '<p>'.self::filterAndUpdate($city['name'], array('codes' => implode(' ', $codes)), $city['plz']);
        }
      }
    }
  }

  static function fixCode($code) {
    $code = preg_replace('/\s+/', ',', preg_replace('/[^0-9]/', ' ' ,trim($code)));
    // if (substr($code, 0, 1) ==  ',') {
    //   $code = substr($code, 1);
    // }
    // if (substr($code, -1, 1) ==  ',') {
    //   $code = substr($code, 0, strlen($code) - 1);
    // }
    return trim($code);
  }

  public static function filterAndUpdate($city_name, $raw_data, $plz)
  {
    $postal_codes = $plz;
    if (empty($raw_data)) {
      $msg = "Given data is empty for city '$city_name'";
    } else if (is_array($raw_data) && array_key_exists('codes', $raw_data)) {
      $postal_codes = $raw_data['codes'];
      $msg = "Postal code found for city '$city_name' : $postal_codes";
    } else if (!property_exists($raw_data[0], 'address_components')) {
      $msg = 'No such property exists "address_components"';
    } else {
      $address_info = $raw_data[0]->address_components;
      foreach($address_info as $info ) {
        if (!empty($info->types) && $info->types[0] == 'postal_code') {
          $postal_codes = is_array($info->long_name) ? implode(',', $info->long_name) : $info->long_name;
          $msg = "Postal code found for city '$city_name' : $postal_codes";
          break;
        }
      }
    }
    if ($postal_codes == $plz) {
      $postal_codes = intval($postal_codes) + 1;
      $msg = "Postal code not found for city '$city_name'";
    }
    // mark as executed so that the script won't get stucked in same data
    DataTable::updateCode($city_name, self::fixCode($postal_codes));
    return $msg;
  }

  public static function uploadFile($file)
  {
    if (!array_key_exists('name', $file)) {
      return 'Invalid file format.';
    }
    $ext = explode('.', $file['name']);
    $extension = end($ext);
    if (!in_array($extension, self::$allowed_extensions)) {
      return 'Uploaded file format is not allowed.';
    }
    $target = self::$dir.'/uploaded.'.$extension;
    if (file_exists($target)) {
      unlink($target);
    }
    if (move_uploaded_file($file['tmp_name'], $target)) {
      self::save(strtolower(htmlentities($_POST['country'])), $target);
      return 'File has been uploaded successfully.';
    }
    return 'File couldnot be uploaded.';
  }

  public static function getFiles()
  {
    $files = [];
    foreach(self::$allowed_extensions as $ext) {
      if (file_exists(self::$dir.'/uploaded.'.$ext)) {
        $files[] = self::$dir.'/uploaded.'.$ext;
      }
    }
    return $files;
  }
  
  // save contents from excel file to database
  public static function save($country, $filename = null)
  {
    if ($filename == null) {
      $files = self::getFiles();
      if (count($files) == 0) {
        return;
      }
      $filename = $files[0];
    }
    $inputFileType = IOFactory::identify($filename);
    $reader = IOFactory::createReader($inputFileType);
    $reader->setReadDataOnly(true);
    $xcl = $reader->load($filename);
    $worksheet = $xcl->getActiveSheet();
    $raw_sql = [];
    foreach ($worksheet->getRowIterator() as $row) {
        $cellIterator = $row->getCellIterator();
        $cellIterator->setIterateOnlyExistingCells(true);
        $data = new Data();
        $data->set('country', $country);
        foreach ($cellIterator as $cell) {
            $data->set($cell->getColumn(), $cell->getValue());
        }
        if ($data->name != 'name') {
          $raw_sql[] = sprintf("('%s', '%s', '%s', '%s')", $data->name, $data->plz, $data->ortname, $data->country);
          //DataTable::insert($data);
        }
    }
    if (DataTable::insertBulk(implode(',', $raw_sql))) {
      @unlink($filename);
    }
  }
}