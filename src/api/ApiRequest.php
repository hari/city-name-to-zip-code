<?php

/**
 * A base class for both API to interface the requests.
 */
class ApiRequest
{
    private $_api_url = "http://maps.googleapis.com/maps/api/geocode/json?address=%s,%s&sensor=true_or_false";
    private $alternate = 'https://en.wikipedia.org/wiki/%s';
    private $_headers = array();

    public function __construct()
    {

    }
  
    public function addHeader($key, $value)
    {
        $this->_headers[$key] = $value;
    }

    public function removeHeader($key)
    {
        if (!array_key_exists($key, $this->_headers)) {
            return false;
        }
        unset($this->_headers[$key]);
    }

    public function getHeaders()
    {
        return $this->_headers;
    }

    protected function buildQuery($params = array())
    {
        $query = '&';
        if (count($params) > 0) {
            $query = '?';
            foreach ($params as $key => $value) {
                $query .= "$key=$value&";
            }
        }
        //remove last &
        return substr($query, 0, strlen($query) - 1);
    }

    protected function getUrl($city_name, $country_name, $use_alternate)
    {
        if ($use_alternate) {
            return sprintf($this->alternate, $city_name);
        }
        return sprintf($this->_api_url, $city_name, $country_name);
    }

    public function pullData($city_name, $country_name, $use_alternate = false)
    {
        $url = $this->getUrl($city_name, $country_name, $use_alternate);
        $response = Requests::get($url);
        if ($response->status_code != '200') {
            return array();
        }
        if ($use_alternate) {
            if ($response->body == null) {
                return array();
            }
            if (strpos($response->body, '<span class="postal-code">') > 1) {
                $codes = explode('</span>', explode('<span class="postal-code">', $response->body)[1])[0];
                return array('codes' => $codes);
            }
            if (strpos($response->body, 'may refer to') > 1 
                || strpos($response->body, 'For the town, see ') > 1
                || strpos($response->body, 'may mean:') > 1
                || strpos($response->body, '(disambiguation)') > 1) {
                return $this->pullData($city_name.',_'.$country_name, $city_name, $use_alternate);
            }
            return array();
        }
        $response = json_decode($response->body);
        if (property_exists($response, 'results')) {
            return $response->results;
        }
        return array();
    }
    
}
