<?php
  require_once __DIR__ .'/autoload.php';
  require_once __DIR__ .'/vendor/autoload.php';
  require_once __DIR__ .'/src/db_config.php';
  if (isset($_GET['table'])) {
    var_dump(DataTable::create());
  } else {
    echo json_encode(DataTable::all());
  }
?>